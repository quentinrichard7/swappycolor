﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonScript : MonoBehaviour {

    public GameObject terrain;
    public Sprite jaune;
    public Sprite rouge;
    public Sprite bleu;
    public Sprite vert;


    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            bleu_OnClick();
        }
        if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            vert_OnClick();
        }
        if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            jaune_OnClick();
        }
        if (Input.GetKeyDown(KeyCode.DownArrow))
        {
            rouge_OnClick();
        }
    }

    public void jaune_OnClick()
    {
        terrain.GetComponent<SpriteRenderer>().sprite = jaune;
        terrain.GetComponent<TerrainScript>().idColor = 0;
    }

    public void vert_OnClick()
    {
        terrain.GetComponent<SpriteRenderer>().sprite = vert;
        terrain.GetComponent<TerrainScript>().idColor = 1;
    }

    public void rouge_OnClick()
    {
        terrain.GetComponent<SpriteRenderer>().sprite = rouge;
        terrain.GetComponent<TerrainScript>().idColor = 2;
    }

    public void bleu_OnClick()
    {
        terrain.GetComponent<SpriteRenderer>().sprite = bleu;
        terrain.GetComponent<TerrainScript>().idColor = 3;
    }
}
