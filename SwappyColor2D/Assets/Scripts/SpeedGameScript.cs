﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpeedGameScript : MonoBehaviour {

    public float speed = 1;
    public float acceleration = 0.000001f;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        speed += acceleration;
	}

    public void StopMoving()
    {
        speed = 0;
        acceleration = 0;
    }
}
