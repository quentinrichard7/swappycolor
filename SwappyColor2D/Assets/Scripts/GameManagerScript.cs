﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManagerScript : MonoBehaviour {
    public int level = 1;
    public float speed = 1;
    public float acceleration = 0.000001f;

    public string highScoreName;

    public int life;
    public Text tScoreFin;
    public DeathMenu theDeathScreen;

    public GameObject gamespawner;

    private int score=0;
    public Text tScore;
    public Text tHighScore;
    private int highScore = 0;


    // Use this for initialization
    void Start () {
        LoadHighScore();
        GetComponent<AudioSource>().mute = !(PlayerPrefs.GetInt("bSound") == 1);


    }
	
	// Update is called once per frame
	void Update () {
        speed += acceleration;
        if (Input.GetKeyDown(KeyCode.Escape)){
            if (theDeathScreen.gameObject.activeSelf == false) { Death(); }
            else { SceneManager.LoadScene("Accueil"); }
        }    
     }


    public void StopMoving()
    {
        speed = 0;
        acceleration = 0;
        gamespawner.SendMessage("ArreteUpdate");
    }


    void ApplyDamage(int dmg)
    {
        life -= dmg;
        if (life <= 0)
        {
            Death();
        }
    }

    void Death()
    {
        //GetComponent<AudioSource>().mute = true;
        if (GetComponent<AudioSource>() != null)
        {
            
            GetComponent<AudioSource>().mute = true;
        }
        SubmitPlayerScore();
        StopMoving();
        tScoreFin.text = "Score : " + score.ToString();
        //Application.LoadLevel(Application.loadedLevel);
        theDeathScreen.gameObject.SetActive(true);
        //ClearEnemy();
    }


    void ClearEnemy()
    {
        foreach (var item in GameObject.FindGameObjectsWithTag("Enemy"))
        {
            Destroy(item);
        }
    }

    public void UpdateScore(int addScore)
    {
        score += addScore;
        tScore.text = score.ToString();
    }


    private void LoadHighScore()
    {
        
        if (PlayerPrefs.HasKey(highScoreName))
        {
            highScore = PlayerPrefs.GetInt(highScoreName);
            tHighScore.text = "HighScore : " + highScore.ToString();
        }

            
       

    }

    public void SubmitPlayerScore()
    {
        if (score > highScore)
        {
            highScore = score;
            SaveHighScore();
        }
    }

    private void SaveHighScore()
    {

        PlayerPrefs.SetInt(highScoreName, highScore);


    }

    private void LoadOption()
    {

    }
}
