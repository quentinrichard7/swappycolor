﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LifeScript : MonoBehaviour {

    public int life;
    public Text tScore;
    public DeathMenu theDeathScreen;
    private int score;


    void ApplyDamage(int dmg)
    {
        life -= dmg;
        if (life <= 0)
        {
            score = gameObject.GetComponent<ScoreScript>().SubmitPlayerScore();
            gameObject.GetComponent<SpeedGameScript>().StopMoving();
            tScore.text = "Score : " + score.ToString();
            //Application.LoadLevel(Application.loadedLevel);
            theDeathScreen.gameObject.SetActive(true);
            //ClearEnemy();



        }
    }


    void ClearEnemy()
    {
        foreach (var item in GameObject.FindGameObjectsWithTag("Enemy"))
        {
            Destroy(item);
        } 
    }
}
