﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMovingScript : MonoBehaviour {

    public int direction;
    private Vector2 vDirection;
    private float speed=1;
    private Vector2 movement;
    private GameObject gameManager;
    private GameObject heart;

    // Use this for initialization
    void Start () {
        gameManager = GameObject.Find("GameManager");
        heart = GameObject.Find("Heart");

        //switch (direction)
        //{
        //    case 0:
        //        vDirection = new Vector2(0, -1);//vers le bas
        //        break;

        //    case 1:
        //        vDirection = new Vector2(-1, 0);//vers la gauche
        //        break;
        //    case 2:
        //        vDirection = new Vector2(0, 1);//vers le haut
        //        break;
        //    case 3:
        //        vDirection = new Vector2(1, 0);//vers la droite
        //        break;

        //    default:
        //        vDirection = new Vector2(0, 0);
        //        break;
        //}
        vDirection = (heart.transform.position - transform.position).normalized;
    }
	
	// Update is called once per frame
	void Update () {
        movement = gameManager.GetComponent<GameManagerScript>().speed * vDirection;
    }

    void FixedUpdate()
    {
        GetComponent<Rigidbody2D>().velocity = movement;
    }
}
