﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotationSimonScript : MonoBehaviour {
    public bool smooth;
    public float rotationSpeed = 0.1f;
    public float angle;
    public bool randomTime;
    public float minRandom;
    public float maxRandom;
    public float secRotation;
    private bool locker = false;
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if(locker == false)
        {
            if (smooth)
            {
                transform.Rotate(0, 0, rotationSpeed);
            }
            else
            {
                if (randomTime)
                {
                    float sec = Random.Range(minRandom, maxRandom);
                    Debug.Log(sec);
                    StartCoroutine(RotateAfterXSec(sec));
                }
                else
                {
                    StartCoroutine(RotateAfterXSec(secRotation));

                }

            }
        }
        
    }

    IEnumerator RotateAfterXSec(float sec)
    {
        locker = true;
        yield return new WaitForSeconds(sec);
        transform.Rotate(0, 0, angle);
        locker = false;
    }
}
