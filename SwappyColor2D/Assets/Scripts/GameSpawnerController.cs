﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameSpawnerController : MonoBehaviour {

    public GameObject[] spawners;
    public GameObject ennemiPrefab;
    public Sprite[] sprites;

    private int dernierIdCouleur;
    private int dernierIdSpawner;

    private float tempsPasseEnSeconde = 0f;
    private float intervalleEnSeconde = 0.7f;

    private int nombreEnnemiIntervalle = 10;
    private int nombreEnnemiSpawn = 0;

    // Use this for initialization
    void Start () {
        PatternUnFixeAutresRotation();
        //GenereListePattern();

    }

    // Update is called once per frame
    void Update () {

        tempsPasseEnSeconde += Time.deltaTime;

        if (idCouleurs.Count <= 10)
        {
            GenereListePattern();
        }
        else if (intervalleEnSeconde <= tempsPasseEnSeconde)
        {
            tempsPasseEnSeconde = 0;

            int i;
            do
            {
                i = nombresASpawn[0];
                SpawnEnnemi(idCouleurs[0], idSpawners[0]);
                EnleveEnnemiASpawn();
            } while (i > 1);

            if (intervalleEnSeconde > 0.2)
            {
                nombreEnnemiSpawn++;
                if (nombreEnnemiSpawn >= nombreEnnemiIntervalle)
                {
                    intervalleEnSeconde -= 0.005f;
                }
            }
            
        }
		
	}

    void SpawnEnnemi (int idCouleur, int idSpawner)
    {
        float x = spawners[idSpawner].transform.position.x;
        float y = spawners[idSpawner].transform.position.y;
        GameObject ennemi = Instantiate(ennemiPrefab, new Vector3(x, y, 0), Quaternion.identity);

        ennemi.GetComponent<EnemyEntityScript>().idColor = idCouleur;
        ennemi.GetComponent<SpriteRenderer>().sprite = sprites[idCouleur];
        ennemi.GetComponent<EnemyMovingScript>().direction = idSpawner;
    }

    private List<int> idCouleurs = new List<int>();
    private List<int> idSpawners = new List<int>();
    private List<int> nombresASpawn = new List<int>();
    void AjouteEnnemiASpawn (int idCouleur, int idSpawner, int nombreASpawn)
    {
        idCouleurs.Add(idCouleur);
        idSpawners.Add(idSpawner);
        nombresASpawn.Add(nombreASpawn);

        dernierIdCouleur = idCouleur;
        dernierIdSpawner = idSpawner;
    }

    void EnleveEnnemiASpawn ()
    {
        idCouleurs.RemoveAt(0);
        idSpawners.RemoveAt(0);
        nombresASpawn.RemoveAt(0);
    }

    void PatternDebut ()
    {
        AjouteEnnemiASpawn(0, 0, 4);
        AjouteEnnemiASpawn(0, 1, 3);
        AjouteEnnemiASpawn(0, 2, 2);
        AjouteEnnemiASpawn(0, 3, 1);

        AjouteEnnemiASpawn(1, 0, 4);
        AjouteEnnemiASpawn(1, 1, 3);
        AjouteEnnemiASpawn(1, 2, 2);
        AjouteEnnemiASpawn(1, 3, 1);

        AjouteEnnemiASpawn(2, 0, 4);
        AjouteEnnemiASpawn(2, 1, 3);
        AjouteEnnemiASpawn(2, 2, 2);
        AjouteEnnemiASpawn(2, 3, 1);

        AjouteEnnemiASpawn(3, 0, 4);
        AjouteEnnemiASpawn(3, 1, 3);
        AjouteEnnemiASpawn(3, 2, 2);
        AjouteEnnemiASpawn(3, 3, 1);
    }

    private int nombreDeGeneration = 0;
    void GenereListePattern()
    {
        nombreDeGeneration++;

        int nombreEnnemiSpawn = 0;

        while(nombreEnnemiSpawn <= 80*nombreDeGeneration)
        {
            nombreEnnemiSpawn += GetPatternSuivant();
        }

        PatternPause();
    }

    int GetPatternSuivant ()
    {
        int nombreEnnemiSpawn = 0;
        int patternSuivant = Random.Range(0, 5);

        switch (patternSuivant)
        {
            case 0:
                nombreEnnemiSpawn = PatternSpiraleSimple();
                break;

            case 1:
                nombreEnnemiSpawn = PatternDoubleOppose();
                break;

            case 2:
                nombreEnnemiSpawn = PatternRandomSimple();
                break;

            case 3:
                nombreEnnemiSpawn = PatternUnFixeAutresRandom();
                break;

            case 4:
                nombreEnnemiSpawn = PatternUnFixeAutresRotation();
                break;

            default:
                nombreEnnemiSpawn = PatternRandomSimple();
                break;
        }

        nombreEnnemiSpawn += PatternRandomSimple();

        return nombreEnnemiSpawn;
    }

    int GetRandomIDDifferentDernier()
    {
        int id;
        float idf;
        do
        {
            idf = Random.Range(0f, 3.9f);
            id = (int) idf;
        } while (id == dernierIdCouleur);

        return id;
    }

    int PatternPause()
    {
        int idEnnemi = GetRandomIDDifferentDernier();

        for (int i = 0; i < 10; i++)
        {
            AjouteEnnemiASpawn(idEnnemi, 0, 4);
            AjouteEnnemiASpawn(idEnnemi, 1, 3);
            AjouteEnnemiASpawn(idEnnemi, 2, 2);
            AjouteEnnemiASpawn(idEnnemi, 3, 1);
        }

        return 10;
    }

    int PatternSpiraleSimple ()
    {
        int idEnnemi = GetRandomIDDifferentDernier();

        //sens horaire
        if (Random.Range(0f, 1f) > 0.5f)
        {
            int i = 0;
            do
            {
                AjouteEnnemiASpawn(idEnnemi, idEnnemi, 1);
                idEnnemi++;
                if (idEnnemi == 4)
                    idEnnemi = 0;
                //Debug.Log(idPremierEnnemi);
            } while (i++ < 3);
        }
        //sens anti-horaire
        else
        {
            int i = 0;
            do
            {
                AjouteEnnemiASpawn(idEnnemi, idEnnemi, 1);
                idEnnemi--;
                if (idEnnemi == -1)
                    idEnnemi = 3;
            } while (i++ < 3);
        }

        return 4;
    }

    int PatternDoubleOppose()
    {
        int nombreEnnemiSpawn = 0;
        int idEnnemi = GetRandomIDDifferentDernier();

        for (int i = Random.Range(2, 8); i >= 0; i--)
        {
            if(idEnnemi == 0 || idEnnemi == 2)
            {
                AjouteEnnemiASpawn(idEnnemi, 0, 2);
                AjouteEnnemiASpawn(idEnnemi, 2, 1);
            }
            else
            {
                AjouteEnnemiASpawn(idEnnemi, 1, 2);
                AjouteEnnemiASpawn(idEnnemi, 3, 1);
            }

            idEnnemi = GetRandomIDDifferentDernier();
            nombreEnnemiSpawn++;
        }

        return nombreEnnemiSpawn;
    }

    int PatternRandomSimple()
    {
        int nombreEnnemiSpawn = 0;
        int idEnnemi = GetRandomIDDifferentDernier();

        for(int i = Random.Range(0, 4); i >= 0; i--)
        {
            AjouteEnnemiASpawn(idEnnemi, idEnnemi, 1);

            idEnnemi = GetRandomIDDifferentDernier();
            nombreEnnemiSpawn++;
        }

        return nombreEnnemiSpawn;
    }

    int PatternUnFixeAutresRandom()
    {
        int nombreEnnemiSpawn = 0;
        int idEnnemiFixe = GetRandomIDDifferentDernier();
        int idEnnemiAutre;

        for(int i = Random.Range(2, 8); i >= 0; i--)
        {
            AjouteEnnemiASpawn(idEnnemiFixe, idEnnemiFixe, 1);

            idEnnemiAutre = GetRandomIDDifferentDernier();
            AjouteEnnemiASpawn(idEnnemiAutre, idEnnemiAutre, 1);
            nombreEnnemiSpawn++;
        }

        return nombreEnnemiSpawn;
    }

    int PatternUnFixeAutresRotation()
    {
        int nombreEnnemiSpawn = 0;
        int idEnnemiFixe = GetRandomIDDifferentDernier();
        int idEnnemiAutre=idEnnemiFixe;

        //sens horaire
        if (Random.Range(0f, 1f) > 0.5f)
        {
            for (int i = Random.Range(2, 8); i >= 0; i--)
            {
                AjouteEnnemiASpawn(idEnnemiFixe, idEnnemiFixe, 1);

                if (idEnnemiAutre == 3)
                {
                    idEnnemiAutre = 0;
                }
                else
                {
                    idEnnemiAutre = idEnnemiAutre + 1;
                }
                if (idEnnemiFixe == idEnnemiAutre)
                {
                    idEnnemiAutre += 1;
                }
                AjouteEnnemiASpawn(idEnnemiAutre, idEnnemiAutre, 1);
                nombreEnnemiSpawn++;
            }
            
        }
        //sens anti-horaire
        else
        {
            for (int i = Random.Range(2, 8); i >= 0; i--)
            {
                AjouteEnnemiASpawn(idEnnemiFixe, idEnnemiFixe, 1);

                if (idEnnemiAutre == 0)
                {
                    idEnnemiAutre = 3;
                }
                else
                {
                    idEnnemiAutre = idEnnemiAutre - 1;
                }
                if (idEnnemiFixe == idEnnemiAutre)
                {
                    idEnnemiAutre -= 1;
                }
                AjouteEnnemiASpawn(idEnnemiAutre, idEnnemiAutre, 1);
                nombreEnnemiSpawn++;
            }
            
        }

        return nombreEnnemiSpawn;
    }
}
