﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lv2GameSpawnerController : MonoBehaviour
{

    public GameObject[] spawners;
    public GameObject ennemiPrefab;
    public Sprite[] sprites;

    private int dernierIdCouleur;
    private int dernierIdSpawner;

    private float tempsPasseEnSeconde = 0f;
    private float intervalleEnSeconde = 0.4f;

    private int nombreEnnemiIntervalle = 10;
    private int nombreEnnemiSpawn = 0;
    private bool updateEnabled = true;

    // Use this for initialization
    void Start()
    {
        PatternDebut();
        GenereListePattern();
    }

    // Update is called once per frame
    void Update()
    {
        if (updateEnabled)
        {
            tempsPasseEnSeconde += Time.deltaTime;

            if (idCouleurs.Count <= 21 && idCouleurs[0] == 5)
            {
                GenereListePattern();
            }
            else if (intervalleEnSeconde <= tempsPasseEnSeconde)
            {
                tempsPasseEnSeconde = 0;

                int i;
                do
                {
                    i = nombresASpawn[0];
                    SpawnEnnemi(idCouleurs[0], idSpawners[0]);
                    EnleveEnnemiASpawn();
                } while (i > 1);

                if (intervalleEnSeconde > 0.2f)
                {
                    nombreEnnemiSpawn++;
                    if (nombreEnnemiSpawn >= nombreEnnemiIntervalle)
                    {
                        nombreEnnemiSpawn = 0;
                        intervalleEnSeconde -= 0.01f;
                    }
                }

            }
        }

    }

    void ArreteUpdate()
    {
        updateEnabled = false;
    }

    void SpawnEnnemi(int idCouleur, int idSpawner)
    {
        //Debug.Log(idCouleur);
        if (idCouleur < 4)
        {
            float x = spawners[idSpawner].transform.position.x;
            float y = spawners[idSpawner].transform.position.y;
            GameObject ennemi = Instantiate(ennemiPrefab, new Vector3(x, y, 0), Quaternion.identity);

            ennemi.GetComponent<EnemyEntityScript>().idColor = idCouleur;
            ennemi.GetComponent<SpriteRenderer>().sprite = sprites[idCouleur];
            ennemi.GetComponent<EnemyMovingScript>().direction = idSpawner;
        }

    }

    private List<int> idCouleurs = new List<int>();
    private List<int> idSpawners = new List<int>();
    private List<int> nombresASpawn = new List<int>();
    void AjouteEnnemiASpawn(int idCouleur, int idSpawner, int nombreASpawn)
    {
        idCouleurs.Add(idCouleur);
        idSpawners.Add(idSpawner);
        nombresASpawn.Add(nombreASpawn);

        dernierIdCouleur = idCouleur;
        dernierIdSpawner = idSpawner;
    }

    void EnleveEnnemiASpawn()
    {
        idCouleurs.RemoveAt(0);
        idSpawners.RemoveAt(0);
        nombresASpawn.RemoveAt(0);
    }

    void PatternDebut()
    {
        AjouteEnnemiASpawn(0, 0, 4);
        AjouteEnnemiASpawn(0, 1, 3);
        AjouteEnnemiASpawn(0, 2, 2);
        AjouteEnnemiASpawn(0, 3, 1);

        AjouteEnnemiASpawn(1, 0, 4);
        AjouteEnnemiASpawn(1, 1, 3);
        AjouteEnnemiASpawn(1, 2, 2);
        AjouteEnnemiASpawn(1, 3, 1);

        AjouteEnnemiASpawn(2, 0, 4);
        AjouteEnnemiASpawn(2, 1, 3);
        AjouteEnnemiASpawn(2, 2, 2);
        AjouteEnnemiASpawn(2, 3, 1);

        AjouteEnnemiASpawn(3, 0, 4);
        AjouteEnnemiASpawn(3, 1, 3);
        AjouteEnnemiASpawn(3, 2, 2);
        AjouteEnnemiASpawn(3, 3, 1);
    }

    private int nombreDeGeneration = 0;
    void GenereListePattern()
    {
        nombreDeGeneration++;

        int nombreEnnemiSpawn = 0;

        while (nombreEnnemiSpawn <= 80 * nombreDeGeneration)
        {
            nombreEnnemiSpawn += GetPatternSuivant();
        }

        PatternPause();
    }

    int GetPatternSuivant()
    {
        int nombreEnnemiSpawn = 0;
        int patternSuivant = Random.Range(0, 3);

        switch (patternSuivant)
        {
            case 0:
                nombreEnnemiSpawn = PatternSpiraleTwist();
                break;

            case 1:
                nombreEnnemiSpawn = PatternSpiraleRetour();
                break;

            case 2:
                nombreEnnemiSpawn = PatternDuoSpirale();
                break;

            case 3:
                nombreEnnemiSpawn = PatternDuoOpposee();
                break;

            case 4:
                nombreEnnemiSpawn = PatternFlecheRepetee();
                break;

            default:
                nombreEnnemiSpawn = PatternFillerSpirale();
                break;
        }

        nombreEnnemiSpawn += PatternFillerSpirale();

        return nombreEnnemiSpawn;
    }

    int GetRandomIDDifferentDernier()
    {
        int id;
        float idf;
        do
        {
            idf = Random.Range(0f, 3.9f);
            id = (int)idf;
        } while (id == dernierIdCouleur);

        return id;
    }

    int[] GetTableauIDALaSuiteAvecPremierDifferentDernier()
    {
        int[] idCouleurs = new int[4];
        int idActuel = GetRandomIDDifferentDernier();
        float random = Random.Range(0f, 1f);

        if (random > 0.5)
        {
            for (int i = 0; i < 4; i++)
            {
                if (idActuel > 3)
                {
                    idActuel = 0;
                }

                idCouleurs[i] = idActuel;
                idActuel++;
            }
        }
        else
        {
            for (int i = 3; i >= 0; i--)
            {
                if (idActuel > 3)
                {
                    idActuel = 0;
                }

                idCouleurs[i] = idActuel;
                idActuel++;
            }
        }

        return idCouleurs;
    }

    int[] GetTableauIDALaSuiteAvecPremierSuivantDernier()
    {
        int[] idCouleurs = new int[4];
        int idActuel = dernierIdCouleur;
        float random = Random.Range(0f, 1f);

        if (random > 0.5)
        {
            for (int i = 0; i < 4; i++)
            {
                idActuel++;
                if (idActuel > 3)
                {
                    idActuel = 0;
                }

                idCouleurs[i] = idActuel;
            }
        }
        else
        {
            for (int i = 0; i < 4; i++)
            {
                idActuel--;
                if (idActuel < 0)
                {
                    idActuel = 3;
                }

                idCouleurs[i] = idActuel;
            }
        }

        return idCouleurs;
    }

    int PatternPause()
    {
        int idEnnemi = GetRandomIDDifferentDernier();

        AjouteEnnemiASpawn(5, 0, 1);

        for (int i = 0; i < 5; i++)
        {
            AjouteEnnemiASpawn(4, 0, 4);
            AjouteEnnemiASpawn(4, 1, 3);
            AjouteEnnemiASpawn(4, 2, 2);
            AjouteEnnemiASpawn(4, 3, 1);
        }

        return 10;
    }

    //1-2-3-4
    int PatternFillerSpirale()
    {
        int[] idCouleurs = new int[4];
        idCouleurs = GetTableauIDALaSuiteAvecPremierDifferentDernier();

        float randomLimite = Random.Range(0f, 4f);

        for(int i = 0; i < randomLimite; i++)
        {
            for (int j = 0; j < 4; j++)
            {
                AjouteEnnemiASpawn(idCouleurs[j], idCouleurs[j], 1);
            }
        }

        return 4 * (int)randomLimite;

    }

    //1-2-3-4-(3-2-)1-2-3-4-(3-2-)-1-2-3-4
    int PatternSpiraleTwist()
    {
        int[] idCouleurs = new int[4];
        idCouleurs = GetTableauIDALaSuiteAvecPremierSuivantDernier();

        int randomLimite = Random.Range(1, 2);

        for(int i = 0; i < 3; i++)
        {
            for (int j = 0; j < 4; j++)
            {
                AjouteEnnemiASpawn(idCouleurs[j], idCouleurs[j], 1);
            }

            if(i == randomLimite)
            {
                AjouteEnnemiASpawn(idCouleurs[2], idCouleurs[2], 1);
                AjouteEnnemiASpawn(idCouleurs[1], idCouleurs[1], 1);
            }
        }

        return 14;
    }

    //1-2-4-1-3-4-2-3-(+)
    int PatternDuoSpirale()
    {
        int[] idCouleurs = new int[4];
        idCouleurs = GetTableauIDALaSuiteAvecPremierSuivantDernier();

        float randomLimite = Random.Range(0f, 4f);

        for(int i = 0; i < randomLimite; i++)
        {
            AjouteEnnemiASpawn(idCouleurs[0], idCouleurs[0], 1);
            AjouteEnnemiASpawn(idCouleurs[1], idCouleurs[1], 1);

            AjouteEnnemiASpawn(idCouleurs[3], idCouleurs[3], 1);
            AjouteEnnemiASpawn(idCouleurs[0], idCouleurs[0], 1);

            AjouteEnnemiASpawn(idCouleurs[2], idCouleurs[2], 1);
            AjouteEnnemiASpawn(idCouleurs[3], idCouleurs[3], 1);

            AjouteEnnemiASpawn(idCouleurs[1], idCouleurs[1], 1);
            AjouteEnnemiASpawn(idCouleurs[2], idCouleurs[2], 1);
        }

        return 8 * (int)randomLimite;

    }

    //1-2-3-2-1-2-3-(+répétition décalée)
    int PatternSpiraleRetour()
    {
        int[] idCouleurs = new int[4];
        idCouleurs = GetTableauIDALaSuiteAvecPremierSuivantDernier();

        for(int i = 0; i < 4; i++)
        {
            int j = i;
            AjouteEnnemiASpawn(idCouleurs[j], idCouleurs[j], 1);

            for (int k = 0; k < 2; k++)
            {
                j++;
                if (j > 3)
                    j = 0;

                AjouteEnnemiASpawn(idCouleurs[j], idCouleurs[j], 1);
            }

            for (int k = 0; k < 2; k++)
            {
                j--;
                if (j < 0)
                    j = 3;

                AjouteEnnemiASpawn(idCouleurs[j], idCouleurs[j], 1);
            }

            for (int k = 0; k < 2; k++)
            {
                j++;
                if (j > 3)
                    j = 0;

                AjouteEnnemiASpawn(idCouleurs[j], idCouleurs[j], 1);
            }
        }

        return 4 * 7;
    }

    //1-3-1-2-1-4-(+)
    int PatternFlecheRepetee()
    {
        int[] idCouleurs = new int[4];
        idCouleurs = GetTableauIDALaSuiteAvecPremierDifferentDernier();

        float randomLimite = Random.Range(0f, 4f);

        for(int i = 0; i < randomLimite; i++)
        {
            AjouteEnnemiASpawn(idCouleurs[0], idCouleurs[0], 1);
            AjouteEnnemiASpawn(idCouleurs[2], idCouleurs[2], 1);
            AjouteEnnemiASpawn(idCouleurs[0], idCouleurs[0], 1);
            AjouteEnnemiASpawn(idCouleurs[1], idCouleurs[1], 1);
            AjouteEnnemiASpawn(idCouleurs[0], idCouleurs[0], 1);
            AjouteEnnemiASpawn(idCouleurs[3], idCouleurs[3], 1);
        }

        return 6 * (int)randomLimite;

    }

    //1-4-2-3-4-1-3-2
    int PatternDuoOpposee()
    {
        int[] idCouleurs = new int[4];
        idCouleurs = GetTableauIDALaSuiteAvecPremierDifferentDernier();

        float randomLimite = Random.Range(0f, 4f);

        for (int i = 0; i < randomLimite; i++)
        {
            AjouteEnnemiASpawn(idCouleurs[1], idCouleurs[1], 1);
            AjouteEnnemiASpawn(idCouleurs[4], idCouleurs[4], 1);
            AjouteEnnemiASpawn(idCouleurs[2], idCouleurs[2], 1);
            AjouteEnnemiASpawn(idCouleurs[3], idCouleurs[3], 1);
            AjouteEnnemiASpawn(idCouleurs[4], idCouleurs[4], 1);
            AjouteEnnemiASpawn(idCouleurs[1], idCouleurs[1], 1);
            AjouteEnnemiASpawn(idCouleurs[3], idCouleurs[3], 1);
            AjouteEnnemiASpawn(idCouleurs[2], idCouleurs[2], 1);
        }

        return 8 * (int)randomLimite;
    }

}