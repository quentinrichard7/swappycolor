﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotationAroundScript : MonoBehaviour {
    public float speed = 10;
    public GameObject turnAround;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        transform.RotateAround(turnAround.transform.position, new Vector3(0, 0, 1), speed * Time.deltaTime);
    }
}
