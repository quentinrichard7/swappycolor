﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeartScript : MonoBehaviour {

    private GameObject gameManager;
    private bool fx = true;

    // Use this for initialization
    void Start()
    {
        gameManager = GameObject.Find("GameManager");
        fx = PlayerPrefs.GetInt("bFx") == 1;
    }

    void OnTriggerEnter2D(Collider2D coll)
    {
        if (fx == true)
        {
            GetComponent<AudioSource>().Play();
        }
        Destroy(coll.gameObject);
        gameManager.SendMessage("ApplyDamage", 1);
        
    }
}
