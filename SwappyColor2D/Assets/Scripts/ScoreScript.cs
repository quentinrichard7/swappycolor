﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreScript : MonoBehaviour {

    public Text tScore;
    public Text tHighScore;
    private int score = 0;
    private int highScore=0;

    public void Start()
    {
        LoadHighScore();
    }


    public void UpdateScore(int addScore)
    {
        score += addScore;
        tScore.text = score.ToString();
    }


    private  void LoadHighScore()
    {
        if (PlayerPrefs.HasKey("highScore"))
        {
            highScore = PlayerPrefs.GetInt("highScore");
            tHighScore.text = "HighScore : "+highScore.ToString();
        }

    }

    public int SubmitPlayerScore()
    {
        if (score > highScore)
        {
            highScore = score;
            SaveHighScore();
        }
        return score;
    }

    private void SaveHighScore()
    {
        PlayerPrefs.SetInt("highScore", highScore);
    }
}
