﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TerrainScript : MonoBehaviour {

    public int idColor;
    private bool oneFrame;
    private GameObject gameManager;

    private bool fx = true;

    // Use this for initialization
    void Start () {
        gameManager = GameObject.Find("GameManager");
        fx = PlayerPrefs.GetInt("bFx") == 1;
    }
	
	// Update is called once per frame
	void Update () {
        oneFrame= false;
	}

    void OnTriggerStay2D(Collider2D coll) {
        if (coll.gameObject.GetComponent<EnemyEntityScript>().idColor == idColor) {
            gameManager.SendMessage("UpdateScore", 1);
            Destroy(coll.gameObject);
            if (fx == true)
            {
                GetComponent<AudioSource>().Play();
            }
        }

    }
}
