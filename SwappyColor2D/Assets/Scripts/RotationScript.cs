﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotationScript : MonoBehaviour {
    public float rotationSpeed=0.1f;
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        //transform.RotateAround(transform.parent.position, new Vector3(0, 0, 1), 20 * Time.deltaTime);
        transform.Rotate(0, 0, rotationSpeed);

	}
}
