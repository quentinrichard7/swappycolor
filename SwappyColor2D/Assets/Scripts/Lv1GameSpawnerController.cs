﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lv1GameSpawnerController : MonoBehaviour
{

    public GameObject[] spawners;
    public GameObject ennemiPrefab;
    public Sprite[] sprites;

    private int dernierIdCouleur;
    private int dernierIdSpawner;

    private float tempsPasseEnSeconde = 0f;
    private float intervalleEnSeconde = 0.6f;

    private int nombreEnnemiIntervalle = 10;
    private int nombreEnnemiSpawn = 0;
    private bool updateEnabled = true;

    // Use this for initialization
    void Start()
    {
        PatternDebut();
        GenereListePattern();
    }

    // Update is called once per frame
    void Update()
    {
        if (updateEnabled)
        {
            tempsPasseEnSeconde += Time.deltaTime;

            if (idCouleurs.Count <= 21 && idCouleurs[0] == 5)
            {
                GenereListePattern();
            }
            else if (intervalleEnSeconde <= tempsPasseEnSeconde)
            {
                tempsPasseEnSeconde = 0;

                int i;
                do
                {
                    i = nombresASpawn[0];
                    SpawnEnnemi(idCouleurs[0], idSpawners[0]);
                    EnleveEnnemiASpawn();
                } while (i > 1);

                if (intervalleEnSeconde > 0.2f)
                {
                    nombreEnnemiSpawn++;
                    if (nombreEnnemiSpawn >= nombreEnnemiIntervalle)
                    {
                        nombreEnnemiSpawn = 0;
                        intervalleEnSeconde -= 0.01f;
                    }
                }

            }
        }

    }

    void ArreteUpdate()
    {
        updateEnabled = false;
    }

    void SpawnEnnemi(int idCouleur, int idSpawner)
    {
        if (idCouleur < 4)
        {
            float x = spawners[idSpawner].transform.position.x;
            float y = spawners[idSpawner].transform.position.y;
            GameObject ennemi = Instantiate(ennemiPrefab, new Vector3(x, y, 0), Quaternion.identity);

            ennemi.GetComponent<EnemyEntityScript>().idColor = idCouleur;
            ennemi.GetComponent<SpriteRenderer>().sprite = sprites[idCouleur];
            ennemi.GetComponent<EnemyMovingScript>().direction = idSpawner;
        }

    }

    private List<int> idCouleurs = new List<int>();
    private List<int> idSpawners = new List<int>();
    private List<int> nombresASpawn = new List<int>();
    void AjouteEnnemiASpawn(int idCouleur, int idSpawner, int nombreASpawn)
    {
        idCouleurs.Add(idCouleur);
        idSpawners.Add(idSpawner);
        nombresASpawn.Add(nombreASpawn);

        dernierIdCouleur = idCouleur;
        dernierIdSpawner = idSpawner;
    }

    void EnleveEnnemiASpawn()
    {
        idCouleurs.RemoveAt(0);
        idSpawners.RemoveAt(0);
        nombresASpawn.RemoveAt(0);
    }

    void PatternDebut()
    {
        AjouteEnnemiASpawn(0, 0, 4);
        AjouteEnnemiASpawn(0, 1, 3);
        AjouteEnnemiASpawn(0, 2, 2);
        AjouteEnnemiASpawn(0, 3, 1);

        AjouteEnnemiASpawn(1, 0, 4);
        AjouteEnnemiASpawn(1, 1, 3);
        AjouteEnnemiASpawn(1, 2, 2);
        AjouteEnnemiASpawn(1, 3, 1);

        AjouteEnnemiASpawn(2, 0, 4);
        AjouteEnnemiASpawn(2, 1, 3);
        AjouteEnnemiASpawn(2, 2, 2);
        AjouteEnnemiASpawn(2, 3, 1);

        AjouteEnnemiASpawn(3, 0, 4);
        AjouteEnnemiASpawn(3, 1, 3);
        AjouteEnnemiASpawn(3, 2, 2);
        AjouteEnnemiASpawn(3, 3, 1);
    }

    private int nombreDeGeneration = 0;
    void GenereListePattern()
    {
        nombreDeGeneration++;

        int nombreEnnemiSpawn = 0;

        while (nombreEnnemiSpawn <= 80 * nombreDeGeneration)
        {
            nombreEnnemiSpawn += GetPatternSuivant();
        }

        PatternPause();
    }

    int GetPatternSuivant()
    {
        int nombreEnnemiSpawn = 0;
        int patternSuivant = Random.Range(0, 3);

        switch (patternSuivant)
        {
            case 0:
                nombreEnnemiSpawn = PatternSpiraleSimple();
                break;

            case 1:
                nombreEnnemiSpawn = PatternRandomSimple();
                break;

            case 2:
                nombreEnnemiSpawn = PatternSpiraleLong();
                break;

            default:
                nombreEnnemiSpawn = PatternRandomSimple();
                break;
        }

        nombreEnnemiSpawn += PatternRandomSimple();

        return nombreEnnemiSpawn;
    }

    int GetRandomIDDifferentDernier()
    {
        int id;
        float idf;
        do
        {
            idf = Random.Range(0f, 3.9f);
            id = (int)idf;
        } while (id == dernierIdCouleur);

        return id;
    }

    int PatternPause()
    {
        int idEnnemi = GetRandomIDDifferentDernier();

        AjouteEnnemiASpawn(5, 0, 1);

        for (int i = 0; i < 5; i++)
        {
            AjouteEnnemiASpawn(4, 0, 4);
            AjouteEnnemiASpawn(4, 1, 3);
            AjouteEnnemiASpawn(4, 2, 2);
            AjouteEnnemiASpawn(4, 3, 1);
        }

        return 10;
    }

    int PatternSpiraleSimple()
    {
        int idEnnemi = GetRandomIDDifferentDernier();

        //sens horaire
        if (Random.Range(0f, 1f) > 0.5f)
        {
            int i = 0;
            do
            {
                AjouteEnnemiASpawn(idEnnemi, idEnnemi, 1);
                idEnnemi++;
                if (idEnnemi == 4)
                    idEnnemi = 0;
                //Debug.Log(idPremierEnnemi);
            } while (i++ < 3);
        }
        //sens anti-horaire
        else
        {
            int i = 0;
            do
            {
                AjouteEnnemiASpawn(idEnnemi, idEnnemi, 1);
                idEnnemi--;
                if (idEnnemi == -1)
                    idEnnemi = 3;
            } while (i++ < 3);
        }

        return 4;
    }

    int PatternSpiraleLong()
    {
        int idEnnemi = GetRandomIDDifferentDernier();

        //sens horaire
        if (Random.Range(0f, 1f) > 0.5f)
        {
            int i = 0;
            do
            {
                AjouteEnnemiASpawn(idEnnemi, idEnnemi, 1);
                idEnnemi++;
                if (idEnnemi == 4)
                    idEnnemi = 0;
                //Debug.Log(idPremierEnnemi);
            } while (i++ < 11);
        }
        //sens anti-horaire
        else
        {
            int i = 0;
            do
            {
                AjouteEnnemiASpawn(idEnnemi, idEnnemi, 1);
                idEnnemi--;
                if (idEnnemi == -1)
                    idEnnemi = 3;
            } while (i++ < 11);
        }

        return 12;
    }

    int PatternRandomSimple()
    {
        int nombreEnnemiSpawn = 0;
        int idEnnemi = GetRandomIDDifferentDernier();

        for (int i = Random.Range(0, 4); i >= 0; i--)
        {
            AjouteEnnemiASpawn(idEnnemi, idEnnemi, 1);

            idEnnemi = GetRandomIDDifferentDernier();
            nombreEnnemiSpawn++;
        }

        return nombreEnnemiSpawn;
    }

}