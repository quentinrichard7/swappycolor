﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class AccueilController : MonoBehaviour {

    public Text tLevelFocus;
    public Text tHighScoreLvFoc;
    public Image flecheG;
    public Image flecheD;
    public int unlockLvEasy;
    public int unlockLvMedium;
    private bool bLockEasy;
    private bool bLockMedium; 

    public GameObject levelScreen;
    public GameObject optionScreen;
    private bool bOptionOn = false;

    public Toggle soundToggle;
    public Toggle fxToggle;

    public bool bSound = true;
    public bool bFx = true;

    private string LvEditorName;
    private int highScoreLevelFocus;
    private int next=2;
    private int previous=0;

	// Use this for initialization
	void Start () {
        LoadOptionOnStart();
        CheckLevelUnlock();
        FocusLevel1();
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (bOptionOn == true)
            {
                OptionOff();
            }
            else
            {
                Application.Quit();
            }
            
        }
    }

    void CheckLevelUnlock()
    {
        bLockEasy = PlayerPrefs.GetInt("highScore1") > unlockLvEasy ? false : true;
        bLockMedium = PlayerPrefs.GetInt("highScore2") > unlockLvMedium ? false : true;
    }

    void FocusLevel1()
    {
        tLevelFocus.text = "Level 1";
        LvEditorName = "Lv1";

        highScoreLevelFocus = PlayerPrefs.GetInt("highScore1");
        tHighScoreLvFoc.text = "HighScore : " + highScoreLevelFocus.ToString();

        if (bLockEasy)
        {
            flecheD.gameObject.SetActive(false);
            next =0;
        }
        else
        {
            flecheD.gameObject.SetActive(true);
            next = 2;
        }

        previous = 0;
        flecheG.gameObject.SetActive(false);
    }

    void FocusLevel2()
    {
        tLevelFocus.text = "Level 2";
        LvEditorName = "Lv2";
        highScoreLevelFocus = PlayerPrefs.GetInt("highScore2");
        tHighScoreLvFoc.text = "HighScore : " + highScoreLevelFocus.ToString();

        flecheD.gameObject.SetActive(true);
        next = 3;

        previous = 1;
        flecheG.gameObject.SetActive(true);
    }

    void FocusLevelHocusFocus()
    {
        tLevelFocus.text = "Général Hocus Focus";
        LvEditorName = "HocusFocus";
        highScoreLevelFocus = PlayerPrefs.GetInt("highScoreHocusFocus");
        tHighScoreLvFoc.text = "HighScore : " + highScoreLevelFocus.ToString();

        if (bLockMedium)
        {
            flecheD.gameObject.SetActive(false);
            next = 0;
        }
        else
        {
            flecheD.gameObject.SetActive(true);
            next = 4;
        }

        previous = 2;
        flecheG.gameObject.SetActive(true);
    }

    void FocusLevel3()
    {
        tLevelFocus.text = "Level 3";
        LvEditorName = "Lv3";

        highScoreLevelFocus = PlayerPrefs.GetInt("highScore3");
        tHighScoreLvFoc.text = "HighScore : " + highScoreLevelFocus.ToString();

        next = 0;
        previous = 3;
        flecheD.gameObject.SetActive(false);

    }

    public void Previous_OnClick()
    {
        if (previous != 0)
        {
            Focus(previous);
        }
    }

    public void Next_OnClick()
    {
        if (next != 0)
        {
            Focus(next);
        }
    }

    public void Play_OnClick()
    {
        SceneManager.LoadScene(LvEditorName);
    }

    void Focus(int lv)
    {
        Debug.Log(lv);
        switch (lv)
        {
            case 1:
                FocusLevel1();
                break;
            case 2:
                FocusLevel2();
                break;
            case 3:
                FocusLevelHocusFocus();
                break;
            case 4:
                FocusLevel3();
                break;
            default:
                break;
        }
    }

    public void Option_OnClick()
    {
        OptionOn();
        
    }

    void OptionOn()
    {
        levelScreen.SetActive(false);
        optionScreen.SetActive(true);
        bOptionOn = true;
        fxToggle.isOn = bFx;
        soundToggle.isOn = bSound;
    }
    
    public void BackMenu_OnClick()
    {
        OptionOff();
    }

    void OptionOff()
    {
        SaveOption();
        levelScreen.SetActive(true);
        optionScreen.SetActive(false);
        bOptionOn = false;
        
    }

    public void ResetScore()
    {
        PlayerPrefs.SetInt("highScore1", 0);
        PlayerPrefs.SetInt("highScore2", 0);
        PlayerPrefs.SetInt("highScoreHocusFocus", 0);
        PlayerPrefs.SetInt("highScore3", 0);
        FocusLevel1();
    }

    private void LoadOptionOnStart()
    {
        if (PlayerPrefs.HasKey("bFx"))
        {
            bFx = PlayerPrefs.GetInt("bFx") == 1;
            
        }
        else
        {
            PlayerPrefs.SetInt("bFx", 1);
            
        }
        if (PlayerPrefs.HasKey("bSound"))
        {
            bSound = PlayerPrefs.GetInt("bSound") == 1;
            
        }
        else
        {
            PlayerPrefs.SetInt("bSound", 1);
        }


    }

    private void SaveOption()
    {
        PlayerPrefs.SetInt("bFx",bFx ? 1 : 0);
        PlayerPrefs.SetInt("bSound", bSound ? 1 : 0);
    }

    public void SetBoolSound(bool b)
    {
        bSound = b;
    }

    public void SetBoolFx(bool b)
    {
        bFx = b;
    }



}
