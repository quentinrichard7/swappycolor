﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HocusFocusGameSpawnerController : MonoBehaviour
{

    public GameObject[] spawners;
    public GameObject ennemiPrefab;
    public Sprite[] sprites;

    private int dernierIdCouleur = 0;
    private int dernierIdSpawner = 0;
    private int idSpawnerGenere;

    private float tempsPasseEnSeconde = 0f;
    private float intervalleEnSeconde = 0.5f;

    private int nombreEnnemiIntervalle = 10;
    private int nombreEnnemiSpawn = 0;
    private bool updateEnabled = true;

    // Use this for initialization
    void Start()
    {
        GenereListePattern();
    }

    // Update is called once per frame
    void Update()
    {
        if (updateEnabled)
        {
            tempsPasseEnSeconde += Time.deltaTime;

            if (idCouleurs[0] == 5)
            {
                GenereListePattern();
            }
            else if (intervalleEnSeconde <= tempsPasseEnSeconde)
            {
                tempsPasseEnSeconde = 0;

                int i;
                do
                {
                    i = nombresASpawn[0];
                    SpawnEnnemi(idCouleurs[0], idSpawners[0]);
                    EnleveEnnemiASpawn();
                } while (i > 1);

                if (intervalleEnSeconde > 0.2f)
                {
                    nombreEnnemiSpawn++;
                    if (nombreEnnemiSpawn >= nombreEnnemiIntervalle)
                    {
                        nombreEnnemiSpawn = 0;
                        intervalleEnSeconde -= 0.01f;
                    }
                }

            }
        }

    }

    void ArreteUpdate()
    {
        updateEnabled = false;
    }

    void SpawnEnnemi(int idCouleur, int idSpawner)
    {
        //Debug.Log(idCouleur);
        if (idCouleur < 4)
        {
            float x = spawners[idSpawner].transform.position.x;
            float y = spawners[idSpawner].transform.position.y;
            GameObject ennemi = Instantiate(ennemiPrefab, new Vector3(x, y, 0), Quaternion.identity);

            ennemi.GetComponent<EnemyEntityScript>().idColor = idCouleur;
            ennemi.GetComponent<SpriteRenderer>().sprite = sprites[idCouleur];
            ennemi.GetComponent<EnemyMovingScript>().direction = idSpawner;
        }

    }

    private List<int> idCouleurs = new List<int>();
    private List<int> idSpawners = new List<int>();
    private List<int> nombresASpawn = new List<int>();
    void AjouteEnnemiASpawn(int idCouleur, int idSpawner, int nombreASpawn)
    {
        idCouleurs.Add(idCouleur);
        idSpawners.Add(idSpawner);
        nombresASpawn.Add(nombreASpawn);

        dernierIdCouleur = idCouleur;
        dernierIdSpawner = idSpawner;
    }

    void EnleveEnnemiASpawn()
    {
        idCouleurs.RemoveAt(0);
        idSpawners.RemoveAt(0);
        nombresASpawn.RemoveAt(0);
    }

    void PatternDebut()
    {
        AjouteEnnemiASpawn(0, idSpawnerGenere, 1);
        
        AjouteEnnemiASpawn(1, idSpawnerGenere, 1);

        AjouteEnnemiASpawn(2, idSpawnerGenere, 1);

        AjouteEnnemiASpawn(3, idSpawnerGenere, 1);
    }

    private int nombreDeGeneration = 0;
    void GenereListePattern()
    {
        nombreDeGeneration++;

        int nombreEnnemiSpawn = 0;
        float randomIdSpawner = Random.Range(0f, 4f);
        if (randomIdSpawner >= 3.9f)
            randomIdSpawner = 3;
        idSpawnerGenere = (int)randomIdSpawner;

        PatternDebut();

        while (nombreEnnemiSpawn <= 80 * nombreDeGeneration)
        {
            nombreEnnemiSpawn += GetPatternSuivant();
        }

        PatternPause();
    }

    int GetPatternSuivant()
    {
        int nombreEnnemiSpawn = 0;
        int patternSuivant = Random.Range(0, 4);

        switch (patternSuivant)
        {
            case 0:
                nombreEnnemiSpawn = PatternSpiraleTwist();
                break;

            case 1:
                nombreEnnemiSpawn = PatternSpiraleRetour();
                break;

            case 2:
                nombreEnnemiSpawn = PatternSpiraleCroissanteDecroissante();
                break;

            case 3:
                nombreEnnemiSpawn = PatternSpiraleParTrois();
                break;

           default:
                nombreEnnemiSpawn = PatternFillerSpirale();
                break;
        }

        nombreEnnemiSpawn += PatternFillerSpirale();

        return nombreEnnemiSpawn;
    }

    int GetRandomIDDifferentDernier()
    {
        int id;
        float idf;
        do
        {
            idf = Random.Range(0f, 3.9f);
            id = (int)idf;
        } while (id == dernierIdCouleur);

        return id;
    }

    int[] GetTableauIDALaSuiteAvecPremierDifferentDernier()
    {
        int[] idCouleurs = new int[4];
        int idActuel = GetRandomIDDifferentDernier();
        float random = Random.Range(0f, 1f);

        if (random > 0.5)
        {
            for (int i = 0; i < 4; i++)
            {
                if (idActuel > 3)
                {
                    idActuel = 0;
                }

                idCouleurs[i] = idActuel;
                idActuel++;
            }
        }
        else
        {
            for (int i = 3; i >= 0; i--)
            {
                if (idActuel > 3)
                {
                    idActuel = 0;
                }

                idCouleurs[i] = idActuel;
                idActuel++;
            }
        }

        return idCouleurs;
    }

    int[] GetTableauIDALaSuiteAvecPremierSuivantDernier()
    {
        int[] idCouleurs = new int[4];
        int idActuel = dernierIdCouleur;
        float random = Random.Range(0f, 1f);

        if (random > 0.5)
        {
            for (int i = 0; i < 4; i++)
            {
                idActuel++;
                if (idActuel > 3)
                {
                    idActuel = 0;
                }

                idCouleurs[i] = idActuel;
            }
        }
        else
        {
            for (int i = 0; i < 4; i++)
            {
                idActuel--;
                if (idActuel < 0)
                {
                    idActuel = 3;
                }

                idCouleurs[i] = idActuel;
            }
        }

        return idCouleurs;
    }

    int PatternPause()
    {
        int idEnnemi = GetRandomIDDifferentDernier();

        AjouteEnnemiASpawn(5, 0, 1);

        for (int i = 0; i < 5; i++)
        {
            AjouteEnnemiASpawn(4, 0, 4);
            AjouteEnnemiASpawn(4, 1, 3);
            AjouteEnnemiASpawn(4, 2, 2);
            AjouteEnnemiASpawn(4, 3, 1);
        }

        return 10;
    }

    //1-2-3-4
    int PatternFillerSpirale()
    {
        int[] idCouleurs = new int[4];
        idCouleurs = GetTableauIDALaSuiteAvecPremierSuivantDernier();

        float randomLimite = Random.Range(0f, 4f);

        for(int i = 0; i < randomLimite; i++)
        {
            for (int j = 0; j < 4; j++)
            {
                AjouteEnnemiASpawn(idCouleurs[j], idSpawnerGenere, 1);
            }
        }

        return 4 * (int)randomLimite;

    }

    //1-2-3-4-(3-2-)1-2-3-4-(3-2-)-1-2-3-4
    int PatternSpiraleTwist()
    {
        int[] idCouleurs = new int[4];
        idCouleurs = GetTableauIDALaSuiteAvecPremierSuivantDernier();

        int randomLimite = Random.Range(1, 2);

        for(int i = 0; i < 3; i++)
        {
            for (int j = 0; j < 4; j++)
            {
                AjouteEnnemiASpawn(idCouleurs[j], idSpawnerGenere, 1);
            }

            if(i == randomLimite)
            {
                AjouteEnnemiASpawn(idCouleurs[2], idSpawnerGenere, 1);
                AjouteEnnemiASpawn(idCouleurs[1], idSpawnerGenere, 1);
            }
        }

        return 14;
    }

    //1-2-3-2-1-2-3-(+répétition décalée)
    int PatternSpiraleRetour()
    {
        int[] idCouleurs = new int[4];
        idCouleurs = GetTableauIDALaSuiteAvecPremierSuivantDernier();

        for(int i = 0; i < 4; i++)
        {
            int j = i;
            AjouteEnnemiASpawn(idCouleurs[j], idSpawnerGenere, 1);

            for (int k = 0; k < 2; k++)
            {
                j++;
                if (j > 3)
                    j = 0;

                AjouteEnnemiASpawn(idCouleurs[j], idSpawnerGenere, 1);
            }

            for (int k = 0; k < 2; k++)
            {
                j--;
                if (j < 0)
                    j = 3;

                AjouteEnnemiASpawn(idCouleurs[j], idSpawnerGenere, 1);
            }

            for (int k = 0; k < 2; k++)
            {
                j++;
                if (j > 3)
                    j = 0;

                AjouteEnnemiASpawn(idCouleurs[j], idSpawnerGenere, 1);
            }
        }

        return 4 * 7;
    }

    //spirale croissante 1-2-1-2-3-2-1-2-3-4-3-2-1-2-3-2-1-2-1+ spirale décroissante
    int PatternSpiraleCroissanteDecroissante()
    {
        int[] idCouleurs = new int[4];
        idCouleurs = GetTableauIDALaSuiteAvecPremierSuivantDernier();

        float randomLimite = Random.Range(0f, 4f);

        for (int i = 0; i < randomLimite; i++)
        {
            for (int j = 1; j < 4; j++)
            {
                for (int k = 0; k < j; k++)
                {
                    AjouteEnnemiASpawn(idCouleurs[k], idSpawnerGenere, 1);
                }

                for (int k = j; k > 0; k--)
                {
                    AjouteEnnemiASpawn(idCouleurs[k], idSpawnerGenere, 1);
                }
            }

            for (int j = 3; j > 0; j--)
            {
                for (int k = 0; k < j; k++)
                {
                    AjouteEnnemiASpawn(idCouleurs[k], idSpawnerGenere, 1);
                }

                for (int k = j; k > 0; k--)
                {
                    AjouteEnnemiASpawn(idCouleurs[k], idSpawnerGenere, 1);
                }
            }
        }

        AjouteEnnemiASpawn(idCouleurs[0], idSpawnerGenere, 1);

        return ((int)randomLimite * 12) + 1;
    }

    int PatternSpiraleParTrois()
    {
        int[] idCouleurs = new int[5];
        idCouleurs = GetTableauIDALaSuiteAvecPremierSuivantDernier();

        float randomLimite = Random.Range(0f, 12f);

        for (int i = 0; i < randomLimite; i++)
        {
            for (int j = 0; j < 4; j++)
            {
                AjouteEnnemiASpawn(idCouleurs[j], idSpawnerGenere, 1);

                if(j+1 > 3)
                {
                    AjouteEnnemiASpawn(idCouleurs[0], idSpawnerGenere, 1);
                }
                else
                {
                    AjouteEnnemiASpawn(idCouleurs[j + 1], idSpawnerGenere, 1);
                }

                AjouteEnnemiASpawn(idCouleurs[j], idSpawnerGenere, 1);
            }
        }

        return (int)randomLimite * 3;
    }

}